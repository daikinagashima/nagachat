var express = require("express");
var app = express();
var http = require("http").Server(app);
const io = require("socket.io")(http);
const PORT = process.env.PORT || 3000;

//css適用
app.use(express.static("css"));

//html選択
app.get("/", function (req, res) {
  res.sendFile(__dirname + "/chat.html");
});
app.get("/test", function (req, res) {
  res.sendFile(__dirname + "/test.html");
});

io.on("connection", function (socket) {
  socket.on("message", function (msg) {
    console.log("message:" + msg);
    console.log("length:" + msg.length);
    io.emit("message", msg);
  });
});

http.listen(PORT, function () {
  console.log("server listening. Port:" + PORT);
});
